## TODO:

- [x] Tweak color scheme
- [x] Update project photos
- [x] Email contact form
- [x] Swap home and about sections
- [x] Validation for email form? - netlify forms
- [x] Write contact section
- [x] Write about section
- [x] Terminal-esque about section

## BUGS:

- [x] Fix scroll snap active section
- ~~ - [ ] Fix home heading disappearing under navbar when opening console ~~
- [x] Fix fake terminal window buttons